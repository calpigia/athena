#include "tauRec/TauProcessorAlg.h"
#include "tauRec/TauRunnerAlg.h"
#include "tauRec/TauCellThinningAlg.h"
#include "tauRec/ClusterCellRelinkAlg.h"

DECLARE_COMPONENT( TauProcessorAlg )
DECLARE_COMPONENT( TauRunnerAlg )
DECLARE_COMPONENT( TauCellThinningAlg )
DECLARE_COMPONENT( ClusterCellRelinkAlg )
